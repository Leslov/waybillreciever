﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaybillReciever.Models.Database
{
	public class ProductModel : PropertyChangedModel
	{
		public ProductModel() { }
		public ProductModel(string name) => Name = name;

		#region dbProps

		private int _ID;
		public int ID
		{
			get => _ID;
			set { _ID = value; OnPropertyChanged(); }
		}
		private string _Name;
		public string Name
		{
			get => _Name;
			private set { _Name = value; OnPropertyChanged(); }
		}

		internal ProductSaleModel GetActualProductSale(DateTime dt)
		{
			var actualProdSale = ProductSales
					?.Where(x => x.StartDate <= dt && x.EndDate >= dt)
					.OrderByDescending(x => x.StartDate).FirstOrDefault();
			if (actualProdSale == null)
				actualProdSale = new ProductSaleModel
				{
					Product = this,
					StartDate = dt,
				};
			return actualProdSale;
		}
		#endregion

		private List<ProductSaleModel> _ProductSales;
		public List<ProductSaleModel> ProductSales
		{
			get => _ProductSales;
			set { _ProductSales = value; OnPropertyChanged(); }
		}

		public override string ToString() => Name;

		public static ProductModel[] GetSampleProducts()
		{
			var sampleNames = new string[] { "Груша сезонная", "Груша аббат", "Яблоко красное", "Яблоко зеленое" };
			List<ProductModel> products = new List<ProductModel>();
			for (int i = 0; i < sampleNames.Length; i++)
			{
				var prod = new ProductModel
				{
					Name = sampleNames[i],
					ProductSales = ProductSaleModel.GetSampleSales(i).ToList()
				};
				products.Add(prod);
			}
			return products.ToArray();
		}
	}
	public class ProductReport : ProductModel
	{
		public ProductReport() : base() { }
		public ProductReport(string productName, decimal quantity, decimal summ) : base(productName)
		{
			Quantity = quantity;
			Summ = summ;
		}

		public decimal Quantity { get; }
		public decimal Summ { get; }
	}
}