﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaybillReciever.Views;

namespace WaybillReciever.Models.Database
{
	public class WaybillModel : PropertyChangedModel
	{
		#region DbProps
		private int _ID;
		public int ID
		{
			get => _ID;
			set { _ID = value; OnPropertyChanged(); }
		}

		private DateTime _DateCreate = DateTime.UtcNow;
		/// <summary>
		/// Дата создания накладной
		/// </summary>
		[Column(TypeName = "date")]
		public DateTime DateCreate
		{
			get => _DateCreate;
			set
			{
				_DateCreate = value; OnPropertyChanged();
			}
		}

		private int _ShipperID;
		public int ShipperID
		{
			get => _ShipperID;
			set { _ShipperID = value; OnPropertyChanged(); }
		}
		private ShipperModel _Shipper;
		/// <summary>
		/// Отправитель накладной
		/// </summary>
		public ShipperModel Shipper
		{
			get => _Shipper;
			set { _Shipper = value; OnPropertyChanged(); }
		}

		private WaybillPositionModel[] _Positions;
		public virtual WaybillPositionModel[] Positions
		{
			get => _Positions;
			set
			{ _Positions = value; OnPropertyChanged(); }
		}
		#endregion
	}
}