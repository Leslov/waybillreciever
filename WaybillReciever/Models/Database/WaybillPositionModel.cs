﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WaybillReciever.Models.Database
{
	/// <summary>
	/// Позиция в накладной
	/// </summary>
	public class WaybillPositionModel : PropertyChangedModel
	{
		#region DbProps

		private int _ID;
		public int ID
		{
			get => _ID;
			set { _ID = value; OnPropertyChanged(); }
		}

		private decimal _Quantity;
		/// <summary>
		/// Количество
		/// </summary>
		public decimal Quantity
		{
			get => _Quantity;
			set
			{
				_Quantity = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(Summ));
			}
		}

		private int _WaybillID;
		public int WaybillID
		{
			get => _WaybillID;
			set { _WaybillID = value; OnPropertyChanged(); }
		}

		private WaybillModel _Waybill;
		/// <summary>
		/// Накладная
		/// </summary>
		public WaybillModel Waybill
		{
			get => _Waybill;
			set
			{ _Waybill = value; OnPropertyChanged(); }
		}
		private int _ProductSaleID;
		public int ProductSaleID
		{
			get => _ProductSaleID;
			set
			{ _ProductSaleID = value; OnPropertyChanged(); }
		}

		private ProductSaleModel _ProductSale;
		public ProductSaleModel ProductSale
		{
			get => _ProductSale;
			set
			{
				_ProductSale = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(Summ));
			}
		}
		#endregion
		public override string ToString() => ProductSale?.Product?.ToString();

		[NotMapped]
		public decimal? Summ => Math.Round(Quantity * (ProductSale?.Price ?? 0), 2);
	}
}
