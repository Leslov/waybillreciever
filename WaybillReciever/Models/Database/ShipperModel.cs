﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaybillReciever.Views.Windows;

namespace WaybillReciever.Models.Database
{
	/// <summary>
	/// Отправитель
	/// </summary>
	public class ShipperModel : PropertyChangedModel
	{
		public ShipperModel() { }
		public ShipperModel(string name) => Name = name;

		private int _ID;
		public int ID
		{
			get => _ID;
			set { _ID = value; OnPropertyChanged(); }
		}
		/// <summary>
		/// Название отправителя/организации
		/// </summary>
		private string _Name;
		public string Name
		{
			get => _Name;
			set { _Name = value; OnPropertyChanged(); }
		}

		public static string[] GetShipperNames() => new string[] { "Петя", "Вася", "Коля" };
		public static ShipperModel[] GetSampleShippers()
		{
			var sampleNames = new string[] { "Петя", "Вася", "Коля" };
			return sampleNames.Select(x => new ShipperModel(x)).ToArray();
		}
		public override string ToString() => Name;
	}
}
