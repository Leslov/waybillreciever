﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;

namespace WaybillReciever.Models.Database
{
	public class ProductSaleModel : PropertyChangedModel
	{
		private int _ID;
		public int ID
		{
			get => _ID;
			set { _ID = value; OnPropertyChanged(); }
		}

		private DateTime _StartDate;
		public DateTime StartDate
		{
			get => _StartDate;
			set { _StartDate = value; OnPropertyChanged(); }
		}

		private DateTime _EndDate;
		public DateTime EndDate
		{
			get => _EndDate;
			set { _EndDate = value; OnPropertyChanged(); }
		}

		private decimal _Price;
		public decimal Price
		{
			get => _Price;
			set { _Price = value; OnPropertyChanged(); }
		}

		private int _ProductID;
		public int ProductID
		{
			get => _ProductID;
			set { _ProductID = value; OnPropertyChanged(); }
		}
		private ProductModel _Product;
		public ProductModel Product
		{
			get => _Product;
			set { _Product = value; OnPropertyChanged(); }
		}

		internal static ProductSaleModel[] GetSampleSales(int seed)
		{
			int count = 3;
			var result = new List<ProductSaleModel>();
			for (int i = 0; i < count; i++)
			{
				Random rand = new Random(seed * 999 + i);
				result.Add(new ProductSaleModel
				{
					Price = rand.Next(50, 150),
					StartDate = DateTime.Now.Date.AddDays(-40 * i - 10),
					EndDate = DateTime.Now.Date.AddDays(-20 * i + 30),
				});
			}
			return result.ToArray();
		}
	}
}