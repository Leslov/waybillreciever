﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WaybillReciever.Models;
using WaybillReciever.Models.Database;
using WaybillReciever.Services;
using WaybillReciever.Views;
using WaybillReciever.Views.Windows;

namespace WaybillReciever.ViewModels
{
	internal class CreateWaybillViewModel : PropertyChangedModel
	{
		public Command CancelCommand => new Command((_) => Close());
		public Command SaveCommand => new Command((_) =>
		{
			if (CurrentWaybill.Save())
				Close();
		});


		public Command AddPosition => new Command((_) => CurrentWaybill.AddPosition());
		public Command DeletePosition => new Command((param) => CurrentWaybill.DeletePosition(param as WaybillPositionVM));


		/// <summary>
		/// Дата создания накладной
		/// </summary>
		public DateTime? DateCreate
		{
			get => CurrentWaybill.DateCreate;
			set
			{
				if (value.HasValue)
				{
					CurrentWaybill.DateCreate = value.Value;
					CurrentWaybill.DateChanged();
					OnPropertyChanged();
				}
			}
		}
		private WaybillVM _CurrentWaybill = new WaybillVM();
		public WaybillVM CurrentWaybill
		{
			get => _CurrentWaybill;
			set { _CurrentWaybill = value; OnPropertyChanged(); }
		}

		private ProductModel[] _Products;
		public ProductModel[] Products
		{
			get
			{
				if (_Products == null)
					using (var context = Context.GetNewContext())
						_Products = context.GetProducts();
				return _Products;
			}
		}

		private ShipperModel[] _Shippers;
		public ShipperModel[] Shippers
		{
			get
			{
				if (_Shippers == null)
					using (var context = Context.GetNewContext())
						_Shippers = context.GetShippers();
				return _Shippers;
			}
		}

		private void Close() => DisplayWindow.SetActiveUC(null);
	}

	[NotMapped]
	public class WaybillVM : WaybillModel
	{
		private ObservableCollection<WaybillPositionVM> _Positions = new ObservableCollection<WaybillPositionVM>();
		public virtual ObservableCollection<WaybillPositionVM> PositionsVM
		{
			get => _Positions;
			set { _Positions = value; OnPropertyChanged(); }
		}

		public bool Save()
		{
			if (!IsValid())
				return false;
			try
			{
				WaybillModel wbModel = this.ToModel();
				using (var context = Context.GetNewContext())
				{
					context.WaybillPosition.AddRange(wbModel.Positions);
					context.SaveChanges();
					return true;
				}
			}
			catch (Exception ex)
			{
				MessageService.Default.ShowMessage($"{ex.Message}{Environment.NewLine}{ex.InnerException?.Message}{Environment.NewLine}{ex.InnerException?.InnerException?.Message}");
				return false;
			}
		}

		private bool IsValid()
		{
			if (Shipper == null)
			{
				MessageService.Default.ShowMessage("Не выбран поставщик");
				return false;
			}
			if (!PositionsVM.Any())
			{
				MessageService.Default.ShowMessage("Не добавлено ни одной позиции");
				return false;
			}
			if (PositionsVM.Any(x => x.ProductSale == null))
			{
				MessageService.Default.ShowMessage("Не у всех позиций выбран тип продукции");
				return false;
			}
			return true;
		}
		private WaybillModel ToModel()
		{
			var wb = new WaybillModel
			{
				DateCreate = DateCreate,
				ShipperID = Shipper.ID
			};
			List<WaybillPositionModel> positions = new List<WaybillPositionModel>();
			foreach (var position in PositionsVM)
			{
				positions.Add(
					new WaybillPositionModel
					{
						ProductSaleID = position.ProductSale.ID,
						Quantity = position.Quantity,
						Waybill = wb
					});
			}
			wb.Positions = positions.ToArray();
			return wb;
		}

		internal void AddPosition() => PositionsVM.Add(new WaybillPositionVM { Waybill = this });
		internal void DeletePosition(WaybillPositionVM position)
		{
			if (PositionsVM.Contains(position))
				PositionsVM.Remove(position);
		}

		internal void DateChanged()
		{
			foreach (var position in PositionsVM)
				position.ProductPropertyChanged();
		}
	}

	[NotMapped]
	public class WaybillPositionVM : WaybillPositionModel
	{
		public ProductModel Product
		{
			get => ProductSale?.Product;
			set
			{
				UpdateProduct(value);
				OnPropertyChanged();
			}
		}
		private void UpdateProduct(ProductModel product = null)
		{
			var currentProd = product ?? Product;
			ProductSale = Waybill?.DateCreate is DateTime dt
				? currentProd?.GetActualProductSale(dt)
				: null;
		}
		public void ProductPropertyChanged()
		{
			UpdateProduct();
			OnPropertyChanged(nameof(Product));
		}
	}
}