﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaybillReciever.Views;
using WaybillReciever.Views.Windows;

namespace WaybillReciever.ViewModels
{
	internal class MainViewModel : PropertyChangedModel
	{
		public Command NewWaybill => new Command((_) => DisplayWindow.SetActiveUC(new CreateWaybill()));

		public Command Reports => new Command((_) => DisplayWindow.SetActiveUC(new ReportList()));
	}
}
