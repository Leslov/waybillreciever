﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WaybillReciever.Models;
using WaybillReciever.Models.Database;
using WaybillReciever.Views;
using WaybillReciever.Views.Windows;

namespace WaybillReciever.ViewModels
{
	public class ReportListViewModel : PropertyChangedModel
	{
		public ReportListViewModel() { }

		public Command SetSelectedShipper => new Command((param) =>
		{
			SelectedShipper = param as ShipperReport ?? throw new ArgumentNullException(nameof(ShipperReport));
		});

		private ShipperReport _SelectedShipper;
		public ShipperReport SelectedShipper
		{
			get => _SelectedShipper;
			set { _SelectedShipper = value; OnPropertyChanged(); }
		}

		private ShipperReport[] _Shippers;
		public ShipperReport[] Shippers
		{
			get
			{
				if (_Shippers == null)
					using (var context = Context.GetNewContext())
						_Shippers = context.GetShippers().Select(x => new ShipperReport(x.Name)).ToArray();
				return _Shippers;
			}
		}

		private ShipperReport _Total;
		public ShipperReport Total
		{
			get
			{
				if (_Total == null && Shippers.Any())
				{
					_Total = new ShipperReport
					{
						Name = "Итого"
					};
					var products = Shippers.SelectMany(x => x.Products).GroupBy(x => x.Name)
						.Select(x => new ProductReport(x.Key, x.Sum(y => y.Quantity), x.Sum(y => y.Summ))).ToArray();
					_Total.Products = products;
				}
				return _Total;
			}
		}

		private DateTime? _StartDate;
		public DateTime? StartDate
		{
			get => _StartDate;
			set
			{
				_StartDate = value;
				OnPropertyChanged();
				SetDateFilter();
			}
		}

		private DateTime? _EndDate;
		public DateTime? EndDate
		{
			get => _EndDate;
			set
			{
				_EndDate = value;
				OnPropertyChanged();
				SetDateFilter();
			}
		}
		private void ResetTotal()
		{
			_Total = null;
			OnPropertyChanged(nameof(Total));
		}

		private void SetDateFilter()
		{
			foreach (var shipper in Shippers)
				shipper.Init(StartDate, EndDate);
			ResetTotal();
		}
	}
	public class ShipperReport : ShipperModel
	{
		public ShipperReport() : base() { }
		public ShipperReport(string name) : base(name) => Init();

		public void Init(DateTime? startDate = null, DateTime? endDate = null)
		{
			using (var context = Context.GetNewContext())
			{
				var waybillPositions = context.WaybillPosition
					.Include(nameof(WaybillPositionModel.Waybill))
					.Include($"{nameof(WaybillPositionModel.Waybill)}.{nameof(WaybillModel.Shipper)}")
					.Include(nameof(WaybillPositionModel.ProductSale))
					.Include($"{nameof(WaybillPositionModel.ProductSale)}.{nameof(ProductSaleModel.Product)}")
					.Where(x => x.Waybill.Shipper.Name == Name);

				if (startDate.HasValue)
					waybillPositions = waybillPositions.Where(x => x.Waybill.DateCreate > startDate);
				if (endDate.HasValue)
					waybillPositions = waybillPositions.Where(x => x.Waybill.DateCreate < endDate);
				var positions = waybillPositions.ToArray();
				Dictionary<ProductModel, WaybillPositionModel[]> dictionary = positions.GroupBy(x => x.ProductSale.Product).ToArray().ToDictionary(x => x.Key, x => x.ToArray());

				List<ProductReport> productReports = new List<ProductReport>();
				foreach (var currentPositions in dictionary)
				{
					var quantity = currentPositions.Value.Sum(x => x.Quantity);
					var summ = Math.Round(currentPositions.Value.Sum(x => x.Quantity * x.ProductSale.Price), 2);
					productReports.Add(new ProductReport(currentPositions.Key.Name, quantity, summ));
				}
				Products = productReports.OrderBy(x => x.Name).ToArray();
				OnPropertyChanged(nameof(Description));
			}
		}

		public decimal Sum => Products.Sum(x => x.Summ);
		public decimal Quantity => Products.Sum(x => x.Quantity);

		public string Description => $"{(int)Quantity} шт. {(int)Sum} руб.";
		private ProductReport[] _Products;
		[NotMapped]
		public ProductReport[] Products
		{
			get => _Products;
			set { _Products = value; OnPropertyChanged(); }
		}
	}
}
