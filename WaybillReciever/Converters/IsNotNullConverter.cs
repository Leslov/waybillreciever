﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WaybillReciever.Converters
{
	public class IsNotNullConverter : System.Windows.Data.IValueConverter
	{
		/// <summary>
		/// Возвращает true, если объект не равен null. Можно обратить через параметр
		/// </summary>
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			bool isValueNotNull = value is string ? !string.IsNullOrWhiteSpace(value as string) : value != null;
			bool inverse = parameter?.ToString()?.ToLower() == "true";
			return GetReturnValue(isValueNotNull ^ inverse, targetType);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotSupportedException();
		}

		private static object GetReturnValue(bool value, Type targetType)
		{
			if (targetType == typeof(Visibility))
				return value ? Visibility.Visible : Visibility.Collapsed;
			else
				return value;
		}
	}
}
