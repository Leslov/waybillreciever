﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace WaybillReciever.Services
{
	public class MessageService : IMessageService
	{
		private static IMessageService _default;
		public static IMessageService Default
		{
			get
			{
				if (_default == null)
					_default = new MessageService();
				return _default;
			}
		}
		public void ShowMessage(string message)
		{
			Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() => MessageBox.Show(Application.Current.MainWindow, message)));
		}
	}
	public interface IMessageService
	{
		void ShowMessage(string message);
	}
}
