﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using WaybillReciever.Models.Database;
using WaybillReciever.Services;

namespace WaybillReciever
{
	public class Context : DbContext, IDbContext
	{
		private const string connectionStringName = "DbConnectionString";
		public Context() : base(connectionStringName)
		{
			System.Data.Entity.Database.SetInitializer(new CreateDatabaseIfNotExists<Context>());
		}
		public ProductModel[] GetProducts()
		{
			if (!Product.Any())
			{
				Product.AddRange(ProductModel.GetSampleProducts());
				SaveChanges();
			}
			return Product.Include(nameof(ProductModel.ProductSales)).ToArray();
		}
		public ShipperModel[] GetShippers()
		{
			if (!Shipper.Any())
			{
				Shipper.AddRange(ShipperModel.GetSampleShippers());
				SaveChanges();
			}
			return Shipper.ToArray();
		}

		#region Tables
		public DbSet<WaybillModel> Waybill { get; set; }
		public DbSet<WaybillPositionModel> WaybillPosition { get; set; }
		public DbSet<ProductModel> Product { get; set; }
		public DbSet<ProductSaleModel> ProductSale { get; set; }
		public DbSet<ShipperModel> Shipper { get; set; }
		#endregion
		#region Static
		private static int contextId = 0;
		public static Context GetNewContext(int timeout = 15000)
		{
			try
			{
				if (!initializationTask.Wait(timeout))
					MessageService.Default.ShowMessage("Таймаут инициализации базы данных");
				Debug.WriteLine($"New context {contextId++}");
				return new Context();
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		private static Task initializationTask;
		public static void InitDb()
		{
			initializationTask = new Task(() =>
			{
				try
				{
					System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<Context, Database.MigrationConfiguration>());
					var configuration = new Database.MigrationConfiguration { TargetDatabase = new DbConnectionInfo(connectionStringName) };
					var migrator = new System.Data.Entity.Migrations.DbMigrator(configuration);
					migrator.Update();
				}
				catch (Exception ex)
				{
					MessageService.Default.ShowMessage($"Ошибка инициализации базы данных. {ex.Message} {ex.InnerException?.Message}");
				}
			});
			initializationTask.Start();
		}
		#endregion
	}
	public interface IDbContext
	{
		ProductModel[] GetProducts();
		DbSet<WaybillModel> Waybill { get; set; }
		DbSet<WaybillPositionModel> WaybillPosition { get; set; }
		DbSet<ProductModel> Product { get; set; }
		DbSet<ProductSaleModel> ProductSale { get; set; }
		DbSet<ShipperModel> Shipper { get; set; }
	}
}
