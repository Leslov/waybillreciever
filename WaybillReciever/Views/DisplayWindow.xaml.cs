﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WaybillReciever.ViewModels;
using WaybillReciever.Views;
using WaybillReciever.Views.Elements;
using WaybillReciever.Views.Windows;

namespace WaybillReciever
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class DisplayWindow : Window
	{
		public DisplayWindow()
		{
			InitializeComponent();
			_displayWindow = this;
			//ActiveUC = new Main();
		}

		public UserControl ActiveUC
		{
			get => (Tile)GetValue(ActiveUCProperty);
			set => SetValue(ActiveUCProperty, value);
		}

		public static readonly DependencyProperty ActiveUCProperty =
			 DependencyProperty.Register(nameof(ActiveUC), typeof(UserControl), typeof(DisplayWindow));

		#region Немного костылей и зависимостей

		private static DisplayWindow _displayWindow;
		public static void SetActiveUC(UserControl uc)
		{
			_displayWindow.ActiveUC = uc;
		}
		#endregion

		#region TODO
		public Tile ActiveTile
		{
			get => (Tile)GetValue(ApplyCommandProperty);
			set => SetValue(ApplyCommandProperty, value);
		}

		public static readonly DependencyProperty ApplyCommandProperty =
			 DependencyProperty.Register(nameof(ActiveTile), typeof(Tile), typeof(DisplayWindow));
		public static void SetActiveTile(Tile tile)
		{
			_displayWindow.ActiveTile = tile;
		}
		#endregion
	}
}
