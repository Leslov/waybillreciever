﻿// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WaybillReciever.Views.Elements
{
	/// <summary>
	/// Логика взаимодействия для Tile.xaml
	/// </summary>
	public partial class Tile : UserControl
	{
		public Tile()
		{
			InitializeComponent();
		}
		#region Dependency Properties
		public ICommand Command
		{
			get => (ICommand)GetValue(CommandProperty);
			set => SetValue(CommandProperty, value);
		}
		public static readonly DependencyProperty CommandProperty = DependencyProperty.Register(nameof(Command), typeof(ICommand), typeof(Tile));

		public object CommandParameter
		{
			get => GetValue(CommandParameterProperty);
			set => SetValue(CommandParameterProperty, value);
		}
		public static readonly DependencyProperty CommandParameterProperty = DependencyProperty.Register(nameof(CommandParameter), typeof(object), typeof(Tile));

		public string Title
		{
			get => (string)GetValue(TitleProperty);
			set => SetValue(TitleProperty, value);
		}
		public static readonly DependencyProperty TitleProperty = DependencyProperty.Register(nameof(Title), typeof(string), typeof(Tile), new UIPropertyMetadata("Безымянная плитка"));

		public string Description
		{
			get => (string)GetValue(DescriptionProperty);
			set => SetValue(DescriptionProperty, value);
		}
		public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register(nameof(Description), typeof(string), typeof(Tile), new UIPropertyMetadata("Описание"));
		public PackIconKind Kind
		{
			get => (PackIconKind)GetValue(KindProperty);
			set => SetValue(KindProperty, value);
		}
		public static readonly DependencyProperty KindProperty = DependencyProperty.Register(nameof(Kind), typeof(PackIconKind), typeof(Tile), new UIPropertyMetadata(PackIconKind.None));
		#endregion

		private void UserControl_MouseUp(object sender, MouseButtonEventArgs e)
		{
			if (Command?.CanExecute(CommandParameter) == true)
				Command.Execute(CommandParameter);
		}
	}
}
